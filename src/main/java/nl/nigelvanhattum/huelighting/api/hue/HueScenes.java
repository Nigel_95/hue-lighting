package nl.nigelvanhattum.huelighting.api.hue;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import nl.nigelvanhattum.huelighting.model.Scene;
import nl.nigelvanhattum.huelighting.model.State;
import nl.nigelvanhattum.util.http.HTTPHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static nl.nigelvanhattum.huelighting.api.hue.ApiOptions.LIGHTS;
import static nl.nigelvanhattum.huelighting.api.hue.ApiOptions.SCENES;

public class HueScenes {
    private String hueBaseIP, hueUsername;

    public HueScenes(String hueBaseIP, String hueUsername) {
        this.hueBaseIP = hueBaseIP;
        this.hueUsername = hueUsername;
    }

    public String getScenes() {
        String response = HTTPHandler.doGet(String.format("%s/%s/%s", hueBaseIP, hueUsername, SCENES.label));
        return response;
    }

    public String getScenes(String scene_id) {
        String response = HTTPHandler.doGet(String.format("%s/%s/%s/%s", hueBaseIP, hueUsername, SCENES.label, scene_id));
        return response;
    }

    public Scene getScene(String sceneName) {
        String response = getScenes();
        JsonObject object = (JsonObject) new JsonParser().parse(response);

        Set<Map.Entry<String, JsonElement>> entries = object.entrySet();
        for (Map.Entry<String, JsonElement> entry: entries) {
            GsonBuilder builder = new GsonBuilder();
            Scene scene = builder.create().fromJson(entry.getValue(), Scene.class);

            if (sceneName.toLowerCase().equals(scene.getName().toLowerCase())) {
                scene.setId(entry.getKey());

                scene.setLightstates(getStatesByScene(scene));

                return scene;
            }
        }
        return null;
    }

    public HashMap<String, State> getStatesByScene(Scene scene) {
        String response = getScenes(scene.getId());
        JsonObject object = (JsonObject) new JsonParser().parse(response);
        JsonObject lightStates = object.getAsJsonObject("lightstates");

        HashMap<String, State> return_value = new HashMap<>();

        Set<Map.Entry<String, JsonElement>> entries = lightStates.entrySet();
        for (Map.Entry<String, JsonElement> entry: entries) {
            GsonBuilder builder = new GsonBuilder();
            State state = builder.create().fromJson(entry.getValue(), State.class);
            return_value.put(entry.getKey(), state);
        }

        return return_value;
    }

    public String buildLightStateJson(State state) {
        GsonBuilder builder = new GsonBuilder();
        String json = builder.create().toJson(state, State.class);
        return json;
    }

    public void setScene(String sceneName) {
        Scene scene = getScene(sceneName);
        ArrayList<Thread> threads = new ArrayList<>();

        // prepare all JSON strings...
        for (Map.Entry<String, State> entry : scene.getLightstates().entrySet()) {
            String jsonMessage = buildLightStateJson(entry.getValue());
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    HTTPHandler.doRequest(String.format("%s/%s/%s/%s/state", hueBaseIP, hueUsername, LIGHTS.label, entry.getKey()), HTTPHandler.HTTPMETHOD.PUT.name(), jsonMessage);
                }
            });
            threads.add(thread);
        }

        // Execute Threads...
        for(Thread thread : threads) {
            thread.start();
        }


    }
}
