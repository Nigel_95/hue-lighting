package nl.nigelvanhattum.huelighting.api.hue;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import nl.nigelvanhattum.huelighting.model.Group;
import nl.nigelvanhattum.huelighting.model.Light;
import nl.nigelvanhattum.util.http.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static nl.nigelvanhattum.huelighting.api.hue.ApiOptions.GROUPS;
import static nl.nigelvanhattum.huelighting.api.hue.ApiOptions.LIGHTS;

public class HueGroups {
    private String hueBaseIP, hueUsername;

    public HueGroups(String hueBaseIP, String hueUsername) {
        this.hueBaseIP = hueBaseIP;
        this.hueUsername = hueUsername;
    }

    public String getGroups() {
        HTTPHandler httpHandler;
        String response = HTTPHandler.doGet(String.format("%s/%s/%s", hueBaseIP, hueUsername, GROUPS.label));
        return response;
    }

    public Group getGroup(String groupname) {
        String response = getGroups();
        JsonObject object = (JsonObject) new JsonParser().parse(response);

        Set<Map.Entry<String, JsonElement>> entries = object.entrySet();
        for (Map.Entry<String, JsonElement> entry: entries) {
            GsonBuilder builder = new GsonBuilder();
            Group group = builder.create().fromJson(entry.getValue(), Group.class);

            if (groupname.toLowerCase().equals(group.getName().toLowerCase())) {
                return group;
            }
        }
        throw new RuntimeException(String.format("Cannot find %s group, check your spelling?", groupname));
    }

    public HashMap<String, Light> getLightsFromGroup(Group group) {
        List<String> light_ids = group.getLights();
        HashMap<String, Light> lights = new HashMap<>();

        for(String id : light_ids) {
            String json = HTTPHandler.doGet(String.format("%s/%s/%s/%s", hueBaseIP, hueUsername,LIGHTS.label, id));
            GsonBuilder builder = new GsonBuilder();
            Light light = builder.create().fromJson(json, Light.class);
            lights.put(id, light);
        }
        return lights;
    }

}
