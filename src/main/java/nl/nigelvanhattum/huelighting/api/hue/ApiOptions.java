package nl.nigelvanhattum.huelighting.api.hue;

public enum ApiOptions {
        GROUPS ("groups"), LIGHTS("lights"), STATE("state"), SCENES ("scenes");

        public final String label;

        ApiOptions(String label) {
            this.label = label;
        }
}
