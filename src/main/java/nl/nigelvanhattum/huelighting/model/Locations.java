package nl.nigelvanhattum.huelighting.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Locations {

    @SerializedName("1")
    @Expose
    private List<Double> _1 = null;
    @SerializedName("2")
    @Expose
    private List<Double> _2 = null;
    @SerializedName("3")
    @Expose
    private List<Double> _3 = null;

    public List<Double> get1() {
        return _1;
    }

    public void set1(List<Double> _1) {
        this._1 = _1;
    }

    public List<Double> get2() {
        return _2;
    }

    public void set2(List<Double> _2) {
        this._2 = _2;
    }

    public List<Double> get3() {
        return _3;
    }

    public void set3(List<Double> _3) {
        this._3 = _3;
    }

}
