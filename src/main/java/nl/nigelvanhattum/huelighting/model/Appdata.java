
package nl.nigelvanhattum.huelighting.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Appdata {

    @SerializedName("version")
    @Expose
    private Integer version;
    @SerializedName("data")
    @Expose
    private String data;

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

}
