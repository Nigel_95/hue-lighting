package nl.nigelvanhattum.huelighting.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Swupdate {

    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("lastinstall")
    @Expose
    private String lastinstall;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getLastinstall() {
        return lastinstall;
    }

    public void setLastinstall(String lastinstall) {
        this.lastinstall = lastinstall;
    }

}
