package nl.nigelvanhattum.huelighting.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Stream {

    @SerializedName("proxymode")
    @Expose
    private String proxymode;
    @SerializedName("proxynode")
    @Expose
    private String proxynode;
    @SerializedName("active")
    @Expose
    private Boolean active;
    @SerializedName("owner")
    @Expose
    private Object owner;

    public String getProxymode() {
        return proxymode;
    }

    public void setProxymode(String proxymode) {
        this.proxymode = proxymode;
    }

    public String getProxynode() {
        return proxynode;
    }

    public void setProxynode(String proxynode) {
        this.proxynode = proxynode;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Object getOwner() {
        return owner;
    }

    public void setOwner(Object owner) {
        this.owner = owner;
    }

}
