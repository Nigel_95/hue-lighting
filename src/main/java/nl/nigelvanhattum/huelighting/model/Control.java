
package nl.nigelvanhattum.huelighting.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Control {

    @SerializedName("mindimlevel")
    @Expose
    private Integer mindimlevel;
    @SerializedName("maxlumen")
    @Expose
    private Integer maxlumen;
    @SerializedName("colorgamuttype")
    @Expose
    private String colorgamuttype;
    @SerializedName("colorgamut")
    @Expose
    private List<List<Double>> colorgamut = null;
    @SerializedName("ct")
    @Expose
    private Ct ct;

    public Integer getMindimlevel() {
        return mindimlevel;
    }

    public void setMindimlevel(Integer mindimlevel) {
        this.mindimlevel = mindimlevel;
    }

    public Integer getMaxlumen() {
        return maxlumen;
    }

    public void setMaxlumen(Integer maxlumen) {
        this.maxlumen = maxlumen;
    }

    public String getColorgamuttype() {
        return colorgamuttype;
    }

    public void setColorgamuttype(String colorgamuttype) {
        this.colorgamuttype = colorgamuttype;
    }

    public List<List<Double>> getColorgamut() {
        return colorgamut;
    }

    public void setColorgamut(List<List<Double>> colorgamut) {
        this.colorgamut = colorgamut;
    }

    public Ct getCt() {
        return ct;
    }

    public void setCt(Ct ct) {
        this.ct = ct;
    }

}
