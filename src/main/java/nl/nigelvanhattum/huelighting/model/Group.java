package nl.nigelvanhattum.huelighting.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Group {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("lights")
    @Expose
    private List<String> lights = null;
    @SerializedName("sensors")
    @Expose
    private List<Object> sensors = null;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("groupState")
    @Expose
    private GroupState groupState;
    @SerializedName("recycle")
    @Expose
    private Boolean recycle;
    @SerializedName("class")
    @Expose
    private String _class;
    @SerializedName("action")
    @Expose
    private Action action;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getLights() {
        return lights;
    }

    public void setLights(List<String> lights) {
        this.lights = lights;
    }

    public List<Object> getSensors() {
        return sensors;
    }

    public void setSensors(List<Object> sensors) {
        this.sensors = sensors;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public GroupState getGroupState() {
        return groupState;
    }

    public void setGroupState(GroupState groupState) {
        this.groupState = groupState;
    }

    public Boolean getRecycle() {
        return recycle;
    }

    public void setRecycle(Boolean recycle) {
        this.recycle = recycle;
    }

    public String getClass_() {
        return _class;
    }

    public void setClass_(String _class) {
        this._class = _class;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

}
