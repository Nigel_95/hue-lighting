package nl.nigelvanhattum.huelighting.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GroupState {

    @SerializedName("all_on")
    @Expose
    private Boolean allOn;
    @SerializedName("any_on")
    @Expose
    private Boolean anyOn;

    public Boolean getAllOn() {
        return allOn;
    }

    public void setAllOn(Boolean allOn) {
        this.allOn = allOn;
    }

    public Boolean getAnyOn() {
        return anyOn;
    }

    public void setAnyOn(Boolean anyOn) {
        this.anyOn = anyOn;
    }

}
