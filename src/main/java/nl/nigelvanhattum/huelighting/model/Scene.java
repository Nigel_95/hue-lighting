package nl.nigelvanhattum.huelighting.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.List;

public class Scene {

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String id;

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("group")
    @Expose
    private String group;
    @SerializedName("lights")
    @Expose
    private List<String> lights = null;
    @SerializedName("owner")
    @Expose
    private String owner;
    @SerializedName("recycle")
    @Expose
    private Boolean recycle;
    @SerializedName("locked")
    @Expose
    private Boolean locked;
    @SerializedName("appdata")
    @Expose
    private Appdata appdata;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("lastupdated")
    @Expose
    private String lastupdated;
    @SerializedName("version")
    @Expose
    private Integer version;

    private HashMap<String, State> lightstates = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public List<String> getLights() {
        return lights;
    }

    public void setLights(List<String> lights) {
        this.lights = lights;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Boolean getRecycle() {
        return recycle;
    }

    public void setRecycle(Boolean recycle) {
        this.recycle = recycle;
    }

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    public Appdata getAppdata() {
        return appdata;
    }

    public void setAppdata(Appdata appdata) {
        this.appdata = appdata;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getLastupdated() {
        return lastupdated;
    }

    public void setLastupdated(String lastupdated) {
        this.lastupdated = lastupdated;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public HashMap<String, State> getLightstates() {
        return lightstates;
    }

    public void setLightstates(HashMap<String, State> lightstates) {
        this.lightstates = lightstates;
    }

}
